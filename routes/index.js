const app = require('express')();

let User = require('../models/user.model');

app.get('/', (req, res, next) => {
    User.find({}, (err, users) => {
        if (err) {
            res.status(500).send("Server error");
        } else {
            res.status(200).send(users);
        }
    });
});

module.exports = app;