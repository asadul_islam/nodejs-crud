const app = require('express')();
const jwt = require('jsonwebtoken');

let User = require('../models/user.model');

//finding users from database
async function findUser(query, res) {
    let users = await User.find(query, async(err, user) => {
        if (err) {
            res.status(500).send("Server error");
            return null;
        }

        if (!user) {
            return null;
        } else {
            return user;
        }
    });
    return users;
}

//inserting into database
function createUser(user, res) {
    user.save(user, (err, user) => {
        if (err) {
            res.status(500).send("Server error");
        } else {
            res.status(201).send("Sign up successful");
        }
    });
}

//updating one user
function updateUser(query, user, res) {
    User.updateOne(query, user, (err, user) => {
        if (err) {
            res.status(500).send("Server error");
        } else {
            res.status(200).send("User updated");
        }
    });
}

//deleting one user
function deleteUser(query, res) {
    User.deleteOne(query, (err, user) => {
        if (err) {
            res.status(500).send("Server error");
        } else {
            res.status(200).send("User deleted");
        }
    });
}

//user authentication (check if user is logged in or not)
function isAuthenticated(req, res, next) {
    let token = req.cookies['auth'];
    jwt.verify(token, "JWT Secret Key", (err, decodedToken) => { //verifying jwt token
        if (err || !decodedToken) {
            res.status(401).send("You are not logged in.")
        } else {
            res.locals.userId = decodedToken.id;
            next();
        }
    });
}

//user signup handling
app.post('/signup', async(req, res, next) => {
    if (Object.keys(req.body).length != 4) {
        res.status(422).send("Invalid format");
    } else {
        let newUser = new User({
            name: req.body.name,
            email: req.body.email,
            password: req.body.password
        });

        req.checkBody('name', 'Name is required').notEmpty();
        req.checkBody('email', 'Email is required').notEmpty();
        req.checkBody('email', 'Invalid Email').isEmail();
        req.checkBody('password', 'Password is required').notEmpty();
        req.checkBody('confirmPassword', 'Conform password is required').notEmpty();
        req.checkBody('confirmPassword', "Password don't match").equals(newUser.password);

        let errors = req.validationErrors();
        if (errors) {
            return res.status(422).send(errors);
        }

        let user = await findUser({
            email: req.body.email
        });
        if (user.length > 0) {
            res.status(422).send("Email is exist");
        } else {
            createUser(newUser, res);
        }
    }
});

//signin request handling
app.post('/signin', async(req, res, next) => {
    if (Object.keys(req.body).length != 2) {
        return res.status(422).send("Invalid format");
    }

    req.checkBody('email', 'Email is required').notEmpty();
    req.checkBody('password', 'Password is required').notEmpty();

    let errors = req.validationErrors();
    if (errors) {
        return res.status(422).send(errors);
    }

    let user = await findUser({
        email: req.body.email
    });
    if (user.length <= 0) {
        res.status(401).send("User not found")
    } else if (user[0].password != req.body.password) {
        res.status(401).send("Password is incorrect")
    } else {
        let token = jwt.sign({ //creating jwt token
            id: user[0]._id
        }, "JWT Secret Key", {
            expiresIn: 60 * 60 * 24 * 1000, //(24 hour) //validity of jwt token 
            algorithm: 'HS256'
        });

        //res.setHeader('x-auth', token); //setting token as response header
        res.cookie("auth", token);
        res.status(200).send("Login successful");
    }
});

//signout from system
app.get('/signout', isAuthenticated, (req, res, next) => {
    res.clearCookie('auth');
    res.status(200).send("You are successfully logged out");
});

//request handling for getting user details
app.get('/dashboard', isAuthenticated, async(req, res, next) => {
    let user = await findUser({
        _id: res.locals.userId
    }, res);
    if (user.length <= 0) {
        res.status(401).send("User Not Found");
    } else {
        res.status(200).send(user[0]);
    }
});

//request handling for updating user details
app.put('/dashboard', isAuthenticated, async(req, res, next) => {
    let user = await findUser({
        _id: res.locals.userId
    }, res);
    if (user.length <= 0) {
        res.status(401).send("User Not Found");
    } else {
        updateUser({
            _id: res.locals.userId
        }, req.body, res);
    }
});

//request handling for deleting user details
app.delete('/dashboard', isAuthenticated, async(req, res, next) => {
    let user = await findUser({
        _id: res.locals.userId
    }, res);
    if (user.length <= 0) {
        res.status(401).send("User Not Found");
    } else {
        res.clearCookie('auth');
        deleteUser({
            _id: res.locals.userId
        }, res);
    }
});

module.exports = app;