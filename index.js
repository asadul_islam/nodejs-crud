const app = require('express')();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cookieSession = require('cookie-session');
const expressValidator = require('express-validator');
const http = require('http');

const index = require('./routes/index');
const user = require('./routes/user');

mongoose.Promise = require('bluebird');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser("MostValuableSecretKey:)"));
app.use(expressValidator());

app.set('trust proxy', 1) // trust first proxy
app.use(cookieSession({
    name: 'auth',
    keys: ["MostValuableSecretKey:)"],
    maxAge: 24 * 60 * 60 * 1000 // 24 hours
}));

app.use('/', index);
app.use('/user', user);
app.use((req, res, next) => {
    res.status(404).send("Page Not Found");
});

const server = http.createServer(app);

app.listen(3000, (err) => {
    if (err) {
        console.log("Error on connecting server");
    } else {
        console.log("Server listening at port 3000");
        mongoose.connect('mongodb://localhost/node-crud', (err) => {
            if (err) {
                console.log('Error on database connection');
            } else {
                console.log('Database connected');
            }
        });
    }
});