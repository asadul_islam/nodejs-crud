# NodeJs CRUD operation

### Simple node CRUD operation application with JWT authentication

## Setup

 `$ git clone https://Asad441@bitbucket.org/Asad441/nodejs-crud.git`

 `$ cd nodejs-crud`

 `$ npm install`

 `$ npm install -g nodemon`

## MongoDB Server

 `$ mongod --dbpath "DB_PATH"`

## Node Development Server

 `$ Node index.js`

 **or**

 `$ nodemon index.js`

 Use postman to test api. Navigate to  *http://localhost:3000/* to test apis.